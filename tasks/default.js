import gulp from 'gulp';

gulp.task('default', gulp.series('stylint', 'stylus', 'template', 'script', 'copy', 'webp', 'svgSprites', 'server', 'watcher'));
gulp.task('build', gulp.parallel('stylint', 'stylus', 'template', 'script', 'copy', 'webp', 'svgSprites'));
