import consultationCategories from '../blocks/consultation-categories/consultation-categories';
import counters from './modules/counters';

const countersCodeHead = `
<script type="text/javascript" data-skip-moving="true">
    var __cs = __cs || [];
    __cs.push(["setCsAccount", "vQxcOWB26gxBg6ncPcDenLgH2Okg6oqT"]);
</script>
<script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js" data-skip-moving="true"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2T2NC1N4DL" data-skip-moving="true"></script>
<script data-skip-moving="true">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'G-2T2NC1N4DL');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(51762461, "init", {
        id:51762461,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
    ym(51762461,'reachGoal','gratitude');
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51762461" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
`;
const countersCodeBody = `
<script data-skip-moving="true">(function(t, p) {window.Marquiz ? Marquiz.add([t, p]) : document.addEventListener('marquizLoaded', function() {Marquiz.add([t, p])})})('Pop', {id: '62a0236139cee5004f8f0d8d', title: 'Пройти тест', text: 'Подойдет ли вам лечение в нашем центре?', delay: 50, textColor: '#fff', bgColor: '#4fb14a', svgColor: '#fff', closeColor: '#ffffff', bonusCount: 2, bonusText: 'Вам доступны бонусы', type: 'full', position: 'position_top'})</script>
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);

consultationCategories();
