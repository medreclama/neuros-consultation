const consultationCategories = () => {
  const block = document.querySelector('.consultation-categories');
  if (!block) return;
  block.addEventListener('click', (event) => {
    if (event.target.classList.contains('consultation-categories__name')) {
      const { classList } = event.target.parentNode;
      const activeClass = 'consultation-categories__list-item--active';
      if (!classList.contains(activeClass)) classList.add(activeClass);
      else classList.remove(activeClass);
    }
  });
};

export default consultationCategories;
